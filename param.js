var app = new Vue({
    el: '#app',
    data: {
      descrAdd: 'New element add',
      descrList: 'Product list',
      show: false,
      errShow: false,
      listEl: {          
          SKU: '',
          name: '',
          price: '',
          unPar: '',
          unParVal: '',
          selected: false
      },
      arr: [], 
      size: {
        h: '',
        w: '',
        l: ''
      }  
           
    },
    
    watch: {
      //save all data
      arr: {
        handler() {          
          localStorage.setItem('arr', JSON.stringify(this.arr));
        },
        deep: true,
      },
    },

    mounted() {
      //load saved data      
      if (localStorage.getItem('arr')) this.arr = JSON.parse(localStorage.getItem('arr'));
    },

    methods: {
      //add new element to list
      add: function (e) {
        if ((this.listEl.SKU == '') || (this.listEl.name == '') || (this.listEl.price == '') || (this.listEl.unPar == '') || (this.listEl.unParVal == '')) {
          this.show = false;
          this.errShow = true;          
        } else {
          if (this.listEl.unPar == 'Box') {
            this.listEl.unParVal = this.size.w + 'x' + this.size.w + 'x' + this.size.w;
          }        
          this.arr.push({selected: this.listEl.selected, sku: this.listEl.SKU, name: this.listEl.name, price: this.listEl.price, unpar: this.listEl.unPar,unparval: this.listEl.unParVal});
          this.show = true;
        }
      },
      //delete all elements
      delall: function (e) {
        this.arr.splice(0, this.arr.length);        
      },
      //delete selected elements
      delSelected: function (e) {
        var i = this.arr.length-1;        
        for (; i >= 0; i--) {          
          if (this.arr[i].selected == true) {
            this.arr.splice(i,1);
          }
        }
      }
                      
    }
    
  })